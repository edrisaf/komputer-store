const elLaptopSection = document.getElementById("laptop-section");
const elPcImage = document.getElementById("laptop-img");
const elPcTitle = document.getElementById("laptop-title");
const elPcText = document.getElementById("laptop-text");
const elPcPrice = document.getElementById("price");
const elPcSelect = document.getElementById("laptop-selector");
const elPcFeature = document.getElementById("feature-list");

//----- Creating laptops and arrays to hold them
const imgArray = ["resources/acer.png", "resources/hp.png",
    "resources/lenovo.png", "resources/samsung.png"];
const priceArray = [3000, 3500, 4000, 4500];
const pcTitleArray = ["Acer Aspire", "HP Spectre", "Lenovo not Yoga", "Samsung Galaxy"];

const acerText = "Powerful and portable, the Aspire 5 laptop delivers on every aspect of everyday computing.";
const hpText = "HP Spectre x360 Convertible 2-in-1 Laptop: Finish work on time with this HP Spectre x360 convertible laptop.";
const lenovoText = "Use it in tent, tablet, laptop, or stand mode. With this sleek, stylish and secure 360˚ convertible, enjoy your favorite multimedia content in HD and with a 10-point touchscreen 11.6” IPS display.";
const samsungText = "Take the magic of QLED and move to the world of computing with the revolutionary Samsung Galaxy Book Flex.";
const pcTextArray = [acerText, hpText, lenovoText, samsungText];

const acerFeatures = ["Intel i5 processor", "HD screen", "slim bezels"];
const hpFeatures = ["Intel i7 processor", "Full HD 1920x1080", "Bezel-less"];
const lenovoFeatures = ["11.6\" HD touchscreen", "Intel i7 processor", "Easy to break"];
const samsungFeatures = ["13.3\" Full HD", "Lightweight", "Intel i7 processor"];
const pcFeatureArray = [acerFeatures, hpFeatures, lenovoFeatures, samsungFeatures];



elPcSelect.addEventListener('change', function (){
    elPcFeature.innerText = "Features:";
    const value = this.value;
    if (value == -1){
        elPcFeature.innerText = "";
        elLaptopSection.style.display = 'none';
    } else {
        const featureList = pcFeatureArray[value];
        for(let i = 0; i < featureList.length; i++){
            const entry = document.createElement('li');
            entry.appendChild(document.createTextNode(featureList[i]));
            elPcFeature.appendChild(entry);
        }
        elPcImage.src = imgArray[value];
        elPcPrice.innerHTML = priceArray[value];
        elPcTitle.innerHTML = pcTitleArray[value];
        elPcText.innerHTML = pcTextArray[value];
        elLaptopSection.style.display = 'flex';
    }
})

