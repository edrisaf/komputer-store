const elBankBalance = document.getElementById("bank-balance");
const elWorkBalance = document.getElementById("work-balance");
const elLaptopPrice = document.getElementById("price");
const elOutstandingAmount = document.getElementById("outstanding-loan-amount");

const elOutstanding = document.getElementById("outstanding-loan");
const elGetLoanBtn = document.getElementById("get-loan-btn");
const elBankBtn = document.getElementById("bank-btn");
const elWorkBtn = document.getElementById("work-btn");
const elRepayBtn = document.getElementById("repay-loan-btn");
const elBuyBtn = document.getElementById("buy-now-btn");

let hasLoan = false;

// click-triggered function to generate 100 kr on each click
elWorkBtn.addEventListener('click', () => {
    let balance = Number(elWorkBalance.innerHTML) + Number(100);
    elWorkBalance.innerHTML = balance;
})

elBankBtn.addEventListener('click', ()  => {
    let workBalance = Number(elWorkBalance.innerHTML);
    if (hasLoan === true){
        alert("You have a loan to pay! 10% of the amount will go towards down payment.");
        let loanPayment = workBalance * 0.1;
        let bankPayment = workBalance - loanPayment;
        elBankBalance.innerHTML = Number(elBankBalance.innerHTML) + Number(bankPayment);
        payLoan(loanPayment);
    } else {
        elBankBalance.innerHTML = Number(elBankBalance.innerHTML) + Number(workBalance);
    }
    elWorkBalance.innerHTML = 0;
})

elGetLoanBtn.addEventListener('click', () => {
    if (hasLoan == true){
        alert("You can only have one active loan at a time!");
    } else {
        const amount = prompt("Please enter the amount you wish to loan:");
        if (amount > Number(elBankBalance.innerHTML)*2){
            alert("You cannot get a loan more than double of your bank balance!")
        }else {
            alert("The amount has been transferred to your bank account!")
            hasLoan = true;
            elBankBalance.innerHTML = Number(elBankBalance.innerHTML) + Number(amount);
            elOutstandingAmount.innerHTML = Number(amount);
            elOutstanding.style.display = 'block';
            elRepayBtn.style.display = 'block';
        }
    }
})

elRepayBtn.addEventListener('click', () => {
    let workBalance = Number(elWorkBalance.innerHTML);
    payLoan(workBalance);
})

function payLoan(amount){
    elOutstandingAmount.innerHTML = Number(elOutstandingAmount.innerHTML) - Number(amount);
    elWorkBalance.innerHTML = 0;
    if (Number(elOutstandingAmount.innerHTML) <= 0){
        elOutstanding.style.display = 'none';
        elRepayBtn.style.display = 'none';
        elOutstandingAmount.innerHTML = 0;
        elWorkBalance.innerHTML = 0;
        hasLoan = false;
        alert("Gz you paid ur loan");
    }

}

elBuyBtn.addEventListener('click', () => {
    let bankBalance = Number(elBankBalance.innerHTML);
    const pcPrice = Number(elLaptopPrice.innerHTML);
    if (bankBalance >= pcPrice){
        elBankBalance.innerHTML = bankBalance - pcPrice;
        alert("Gz you are now the owner of " + elPcTitle.innerHTML);
    } else {
        alert("You do not have enough money to buy this laptop")
    }
})
